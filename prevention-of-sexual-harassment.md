### Question 1
What kinds of behaviour cause sexual harassment?

1. Inappropriate comments
1. Offensive Language
1. Sexual mails
1. Unwanted physical contact
1. Cyberbullying
1. Quid pro Quo
1. Display of explicit images
1. Spreading sexual rumours

### Question 2
What would you do in case you face or witness any incident or repeated incidents of such behaviour?

* Communicate my discomfort - I would tell the person that I am uncomfortable by their actions.
* Document the incidents to report - Document the time and date of the harassment and ask for witnesses's information 
* Report the incident to my employer - Report the incident to the management/HR department of the company.