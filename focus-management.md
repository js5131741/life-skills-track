### Question 1

What is Deep Work?

Deep work is the state of intense and focus on a demanding task without losing focus.

### Question 2

According to author how to do deep work properly, in a few points?

- By scheduling distraction breaks
- Develop a recurring ritual
- A shutdown ritual in the evening

### Question 3

How can you implement the principles in your day to day life?

- By Creating a distraction free environment
- Scheduling deep work sessions
- By establishing routines
- By measuring and reflecting on my work
- Prioritazing tasks

### Question 4

What are the dangers of social media, in brief?

- Development of addicting behavior
- Getting cyberbullied by others
- Stress and feeling inadequate by comparing with others
- Physical health can be affected
- Getting roped into scams
- Loss of productivity by wasting time scrolling
