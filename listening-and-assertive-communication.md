### Question 1
What are the steps/strategies to do Active Listening?
    
1. Avoid Interrupting
2. Door openers
3. Use Body language
4. Take notes
5. Paraphrase
6. Use Silence
7. Reflect Feelings:

### Question 2
According to Fisher's model, what are the key points of Reflective Listening? 

1. Paraphrasing
2. Avoid Judging
3. Showing empathy
4. Using body language

### Question 3
What are the obstacles in your listening process?

I get bored if the speaker is not confident in himself or speaking of irrelevant things. I tend to think on my sometimes and stop my listening.

### Question 4
What can you do to improve your listening?

I could give more attention the the speaker and I should start making notes. I should be more confident and ask questions to the speaker.

### Question 5
When do you switch to Passive communication style in your day to day life?

I switch to it most of the time due to my introverted personality. I dont like to offend other normally as everyone is going through something their life. I also lack confidence in my English so I go with passive communication style.

### Question 6
When do you switch into Aggressive communication styles in your day to day life?

I rarely switch to it but only when someone doesn't respect my boundaries and going out of their way to be rude to me. It's like I  on't start a war but I do bring a good fight to it.

### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I use passive-aggressive communication when I'm upset or annoyed but don't want to confront the issue directly or are afraid of conflict. 


### Question 8
How can you make your communication assertive? 

I should start setting boundaries and exhibit open and approachable body langauge. I should also improve my active listening and be confident in myself.
