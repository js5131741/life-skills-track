### Question 1

Which point(s) were new to you?

- Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup
- Look at the way issues get reported in large open-source projects.

With my past experience, I am already informed on the rest of the points which I have learnt from my team lead.

### Question 2

Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I need to improve on giving my 100% involement. I need to exercise more to keep my energy levels through out the day.
- Explore more on each topics that I learn.
