### Question 1

In this video, what was the most interesting story or idea for you?

Creating lasting behavior change can be achieved by starting with tiny, manageable habits and anchoring them to existing behaviors which will help us eliminate the need for high motivation or willpower.

### Question 2

How can you use B = MAP to make making new habits easier? What are M, A and P.

M - Motivation, A - Ability, P - Prompt
By using all the three, we can make creating new habits more manageable, and increase the change of success.

### Question 3

Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

By making celebration of achievements a habit, even if they are small, we create a positive feedback loop that strengthens the motivation and commitment to larger goals. This concept aligns with the idea that "whatever you celebrate becomes a habit" because celebrating our successes makes it more likely that we'll repeat and master the behaviors over time.

### Question 4

In this video, what was the most interesting story or idea for you?

The idea that habits are formed through small and consistent choices daily and that it transforms us better or worse every day which makes it the compound interest of self improvement.

### Question 5

What is the book's perspective about Identity?

The book promotes that identity is an important part in habit formations. It encourages the readers to align habits to their identities so that it can create lasting positive changes in our lives.

### Question 6

Write about the book's perspective on how to make a habit easier to do?

The book suggests using 'reduce the friction' principle to make a habit easier, which is to leverage the power of simple actions and environment design to minimize the effort required to perform the habit.

### Question 7

Write about the book's perspective on how to make a habit harder to do?

Environment cues, convenience makes it easier for habits to be broken or hard to form.

### Question 8

Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to exercise more. By being comfortable in the PG room and do basic exercises everday and achieve some daily goal will make the habit more attractive.

### Question 9

Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I would like to stop my habit of binge watching series. I should watch couple of episodes, then stop and start something productive or things that will help me long-term.
