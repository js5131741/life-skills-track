### Question 1

Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

Grit is more important for success than just being naturally talented. Even if we are not naturally talented, we can achieve many things with true grit.

### Question 2

Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

Growth mindset is about believing skills can be achieved and developed, and embraces effort, challenges, mistakes, and feedback as opportunities for growth.

### Question 3

What is the Internal Locus of Control? What is the key point in the video?

Internal Locus of Control is achieved by hard work which gives motivation to tackle difficult challenges.
Key point : Develop internal locus of control by taking credit for solving problems which will boost motivation and belief in controlling their own destiny.

### Question 4

What are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Believe in Your Ability to Figure Things Out
- Question your assumptions
- Develop your own life curriculum
- Honor the Struggle

### Question 5

What are your ideas to take action and build Growth Mindset?

- Honor the Growth
- Learn Continuously
- Face the challenges
- Seek feedback
- Learn from failures
- Perseverance
- Set my own goals
