### Question 1

What are the activities you do that make you relax - Calm quadrant?

I like to listen to music, read manga to calm and relax.

### Question 2

When do you find getting into the Stress quadrant?

When I try to solve a problem, finding bugs in the code, trying to win multiplayer games.

### Question 3

How do you understand if you are in the Excitement quadrant?

We will experience restlessness, increased heartrate, will be difficult to concentrate on other things.

### Question 4

Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep is essential for memory formation.
- Lack of sleep will weaken the immune system
- Sleep deprivation impairs memory and learning new things.
- Sleep is a necessity for overall health.

### Question 5

What are some ideas that you can implement to sleep better?

- We can sleep and wake up at the same time everyday.
- Usage of white noise
- Regular exercise
- Limiting exposure to devices 30 minutes before sleep

### Question 6

Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- Exercise improves attention and focus.
- Regular exercise improves mood through release of dopamine and seratonin.
- Long term exercise can improve long term memory.
- It can work as a safeguard and increase resilience against memory decline with aging.
- It can improve reaction times.

### Question 7

What are some steps you can take to exercise more?

- Creating a schedule
- By joining a gym
- Setting realistic expectations and goals
- By incorporating exercise into my daily life
- Reward myself for achieving small goals
