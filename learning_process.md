### Question 1
What is the Feynman Technique? Explain in 1 line.

The ability to understand a concept very well and able to teach it to anyone.

### Question 2

In this video, what was the most interesting story or idea for you?

I found the pomodoro technique the most interesting becasue I am a bit procrastinator myself and I haven't done much about resolving or searching for any ways to get rid of it. Now that I know about this, I would definitely try to incorporate this from now on.

### Question 3

What are active and diffused modes of thinking?

1. Active thinking : Active thinking is when we are focused on a problem and our brain would totally engaged in solving it.
2. Diffused thinking : Diffused thinking is about being in a  relaxed state which allows our brain to work out creative problems in the background and come up with solutions out of nowhere.

### Question 4

According to the video, what are the steps to take when approaching a new topic? Only mention the points.

1. Deconstruct the skill
2. Learn enough to self-correct
3. Remove practice barriers
4. Practice at least 20 hours

### Question 5

What are some of the actions you can take going forward to improve your learning process?

1. Using pomodoro technique to stop procrastinating as much as before.
2. Try to switch between active and diffused modes regularly.
